package ReqRes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class DeleteMethods {

    @Test
    public void deleteUser()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response postResponse = given().header("content-type","application/json").delete("/api/users/2");
        System.out.println("---> Response is = " + postResponse.asString());
        Assert.assertEquals(postResponse.getStatusCode(),204);
    }
}


