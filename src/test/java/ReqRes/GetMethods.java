package ReqRes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class GetMethods {

    @Test
    public void listUsers()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/users?page=2");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),200 );

    }

    @Test
    public void listSingleUser()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/users/2");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),200 );

    }

    @Test
    public void SingleUser_NotFound()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/users/23");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),404 );

    }

    @Test
    public void listResource()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/unknown");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),200 );

    }
    @Test
    public void listSingleResource()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/unknown/2");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),200 );

    }

    @Test
    public void ResourcNotFound()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/unknown/23");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),404 );

    }

    @Test
    public void delayResponse()
    {
        RestAssured.baseURI = "https://reqres.in/";
        Response response = given().get("/api/users?delay=3");
        System.out.println("----> Response is = " + response.asString());
        Assert.assertEquals(response.getStatusCode(),200 );

    }











}
