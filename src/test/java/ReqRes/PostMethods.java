package ReqRes;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class PostMethods {

    @Test
    public void createUser()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("name", "Neha");
        requestParams.put("job", "QA");


        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/api/users");
        System.out.println("---> Response is = " + postResponse.asString());
        Assert.assertEquals(postResponse.getStatusCode(),201 );
    }

    @Test
    public void loginSuccessful()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("email", "eve.holt@reqres.in");
        requestParams.put("password", "cityslicka");


        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/api/login");
        System.out.println("---> Response is = " + postResponse.asPrettyString());
        Assert.assertEquals(postResponse.getStatusCode(),200 );
    }

    @Test
    public void loginUnSuccessful()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("email", "eve.holt@reqres.in");

        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/api/login");
        System.out.println("---> Response is = " + postResponse.asPrettyString());
        Assert.assertEquals(postResponse.getStatusCode(),400 );
    }

    @Test
    public void registerSuccessful()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("email", "eve.holt@reqres.in");
        requestParams.put("password", "pistol");


        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/api/register");
        System.out.println("---> Response is = " + postResponse.asPrettyString());
        Assert.assertEquals(postResponse.getStatusCode(),200 );
    }


    @Test
    public void registerUnSuccessful()
    {
        RestAssured.baseURI = "https://reqres.in/";

        JSONObject requestParams = new JSONObject();
        requestParams.put("password", "pistol");


        Response postResponse = given().header("content-type","application/json").body(requestParams.toString()).post("/api/register");
        System.out.println("---> Response is = " + postResponse.asPrettyString());
        Assert.assertEquals(postResponse.getStatusCode(),400 );
    }




}
